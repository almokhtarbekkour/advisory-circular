#!/bin/bash
set -e
install_dir=/usr/local/share/advisorycircular
config_dir=/etc/advisorycircular
run_dir=/var/lib/advisorycircular
user=advisorycircular

if test `whoami` != root; then echo you must run this as root ; exit 1 ; fi

site="$1"

if test -z "$site" 
then
    echo usage: $0 sitename
    exit 1
fi

# Create install, config, and run directories if they doesn't exist.
echo Creating directories
mkdir -p "$install_dir"
mkdir -p "$config_dir"
mkdir -p "$run_dir"

# Create advisory-circular user if it doesn't exist.
if ! id -u ${user} >/dev/null 2>&1
then
    echo Creating ${user} user
    adduser --system --home ${install_dir} --no-create-home --quiet ${user}
fi

# Compile.
echo Compiling
npm install
npx shadow-cljs compile script

# Install
echo Copying files
install intervalexec.py "$install_dir"/intervalexec
install advisorycircular.sh "$install_dir"/advisorycircular
cp -r node_modules "$install_dir"
cp -r .shadow-cljs "$install_dir"
mkdir -p "$install_dir"/out
cp out/advisorycircular.js "$install_dir"/out

cp config-skel.yaml "$config_dir"
cp secrets-skel.yaml "$config_dir"

cp config.yaml "$config_dir"/"$site"-config.yaml
cp secrets.yaml "$config_dir"/"$site"-secrets.yaml

cp advisorycircular@.service /lib/systemd/system
cp advisorycircular@.service /lib/systemd/system/advisorycircular@"$site".service

# Download aircraft info DB.
echo Downloading aircraft info database
mkdir -p "$install_dir"/aircraft-info
curl 'https://www.mictronics.de/aircraft-database/indexedDB_old.php' > "$install_dir"/aircraft-info/db.zip
(cd "$install_dir"/aircraft-info && unzip -o db.zip)
# Convert JSON to sqlite DB.
node "$install_dir"/out/advisorycircular.js --aircraft-info-db /tmp/aircraft-info-$$.sqb --create-aircraft-info-db-from-json "$install_dir"/aircraft-info/aircrafts.json && mv /tmp/aircraft-info-$$.sqb "$install_dir"/aircraft-info/aircraft-info.sqb

chown -R "$user" "$install_dir"
chown -R "$user" "$config_dir"
chown -R "$user" "$run_dir"

chmod go-r "$config_dir"/secrets*.yaml
chmod go-w "$run_dir"

echo "Installation complete."
echo "You probably want to execute the following:"
echo "sudo systemctl enable advisorycircular@${site}.service"
echo "sudo systemctl start  advisorycircular@${site}.service"