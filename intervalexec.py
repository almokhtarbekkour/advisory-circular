#!/usr/bin/env python3
import subprocess
import sys
import time


def run(args):
    subprocess.run(args, check=True)


def main(args):
    interval_secs = float(args[0])
    exec_args = args[1:]
    while True:
        start_time = time.time()
        next_time = start_time + interval_secs
        run(exec_args)
        now = time.time()
        delay = next_time - now
        if delay > 0.01:
            time.sleep(delay)


if __name__ == '__main__':
    main(sys.argv[1:])
