#!/bin/bash
#
# Compiles and installs a new version of the advisorycircular script
# without downloading and creating a new aircraft info DB.

set -e
install_dir=/usr/local/share/advisorycircular
config_dir=/etc/advisorycircular
run_dir=/var/lib/advisorycircular
user=advisorycircular

# Compile.
echo Compiling
npm install
npx shadow-cljs compile script

# Install
echo Copying files
install intervalexec.py "$install_dir"/intervalexec
install advisorycircular.sh "$install_dir"/advisorycircular
cp -r node_modules "$install_dir"
cp -r .shadow-cljs "$install_dir"
mkdir -p "$install_dir"/out
cp out/advisorycircular.js "$install_dir"/out

# Download aircraft info DB.
# echo Downloading aircraft info database
# mkdir -p "$install_dir"/aircraft-info
# curl 'https://www.mictronics.de/aircraft-database/indexedDB.php' > "$install_dir"/aircraft-info/db.zip
# (cd "$install_dir"/aircraft-info && unzip -o db.zip)
# # Convert JSON to sqlite DB.
# node "$install_dir"/out/advisorycircular.js --aircraft-info-db /tmp/aircraft-info-$$.sqb --create-aircraft-info-db-from-json "$install_dir"/aircraft-info/aircrafts.json && mv /tmp/aircraft-info-$$.sqb "$install_dir"/aircraft-info/aircraft-info.sqb

chown -R "$user" "$install_dir"
chown -R "$user" "$config_dir"
chown -R "$user" "$run_dir"
